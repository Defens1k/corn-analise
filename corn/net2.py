import pathlib

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from math import log2

neurons = 64

df = pd.read_csv('../input/10_ceras.csv')

df = df[df['year'] != 2019]

df.drop(columns = ['quantity_name'], inplace = True)


russian = df[df['country_or_area'] == 'Russian Federation']
all_country = df[df['country_or_area'] != 'Russian Federation']

russian_export = russian[russian['flow'] == 'Export']
all_country_import = all_country[all_country['flow'] == 'Import']

graph_list = {}
def graph_list_commodity():
    for commodity in russian_export['commodity'].unique():
        
        value = russian_export[russian_export['commodity'] == commodity]
        value = value.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')

        import_graph = all_country_import[all_country_import['commodity'] == commodity]
        import_graph = import_graph.groupby(['year'],as_index=False)['weight_kg','quantity'].agg('sum')



        value['quantity'] = import_graph['weight_kg']

        value.rename(columns={'weight_kg': 'russian_export', 'quantity': 'world_import'}, inplace=True)

        graph_list[commodity] = value

graph_list_commodity()

def log_graphs():
    for i in graph_list.keys():
        new_col = list()
        for j in graph_list[i]['russian_export']:
            new_col.append(log2(j))
        graph_list[i]['russian_export'] = new_col
        new_col = list()
        for j in graph_list[i]['world_import']:
            new_col.append(log2(j))
        graph_list[i]['world_import'] = new_col

window_len = 10
def splited():
    count_years = (2018 - 1996 + 1)
    first_year = 1996
    my_index = 0    


    new_pd = pd.DataFrame()
    for i in range(window_len):
        new_pd['r_e-' + str(i)] = i  

    errors = dict()
    for i in graph_list.keys():
        for left in range(count_years - window_len + 1):
            try:
                row = list()
                row_exp = list()
                row_imp = list()
                for pos in range(window_len):
                    row_exp.append(float(graph_list[i][graph_list[i]['year'] == first_year + left + pos]['russian_export']))
                for pos in range(window_len):
                    row_imp.append(float(graph_list[i][graph_list[i]['year'] == first_year + left + pos]['world_import']))
                def norm(input_row):
                    return input_row
                    normed_row = list()
                    maximum = max(input_row)
                    minimum = min(input_row)
                    for col in input_row:
                        normed_row.append((col - minimum) / (maximum - minimum))
                    return normed_row
                row = norm(row_exp)
                new_pd.loc[my_index] = row
                my_index += 1
            except:
                if i in errors.keys():
                    if first_year + left + pos not in errors[i]:

                        errors[i].append(first_year + left + pos)
                else:
                    errors[i] = list()
                    errors[i].append(first_year + left + pos)
    return new_pd



log_graphs()
normed_data = splited()

normed_train = normed_data.sample(frac=0.8,random_state=0)
normed_test = normed_data.drop(normed_train.index)

normed_train_x = normed_train.copy()
normed_train_x.drop(columns = ['r_e-' + str(window_len - 1)], inplace = True)

normed_train_y = normed_train.copy()
for i in range(window_len - 1):
    normed_train_y.drop(columns = ['r_e-' + str(i)], inplace = True)

normed_test_x = normed_test.copy()
normed_test_x.drop(columns = ['r_e-' + str(window_len - 1)], inplace = True)

normed_test_y = normed_test.copy()
for i in range(window_len - 1):
    normed_test_y.drop(columns = ['r_e-' + str(i)], inplace = True)





def build_model():
  model = keras.Sequential([
    layers.Dense(100, activation='sigmoid', input_shape=[len(normed_train_x.keys())]),
    layers.Dense(100, activation='sigmoid'),
    layers.Dense(100, activation='linear'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.01)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mape', 'mae', 'mse'])
  return model

model = build_model()

model.summary()

class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    if epoch % 100 == 0: print('')
    print('.', end='')

EPOCHS = 400

history = model.fit(
  normed_train_x, normed_train_y,
  epochs=EPOCHS, validation_split = 0.2, verbose=0,
  callbacks=[PrintDot()])


hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print(hist.tail(40))

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,30])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Percent Error [MPG]')
  plt.plot(hist['epoch'], hist['mape'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mape'],
           label = 'Val Error')
  plt.ylim([0,100])
  plt.legend()


  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,30])
  plt.legend()
  plt.show()


plot_history(history)


loss, mape, mae, mse = model.evaluate(normed_test_x, normed_test_y, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mape))




exit()




for i in new_pds:
    plt.plot(i)
    plt.show()

for i in graph_list.keys():
    plt.plot(graph_list[i])
    plt.show()











































exit()

df_sum = df_sum.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')

max_kg = df.weight_kg.max()
min_kg = df.weight_kg.min()
new_kg = list()
for i in df['weight_kg']:
  new_kg.append(10 * i / max_kg)
#new_kg = new_kg.sort()
df['weight_kg'] = new_kg

df['weight_kg'].plot(figsize=(12,6))
plt.show()


print("max_kg")
print(max_kg)
print(min_kg)
print("max_kg")


train_dataset = df.sample(frac=0.8,random_state=0)
test_dataset = df.drop(train_dataset.index)


train_input = train_dataset.copy()
train_input.drop(columns = ['weight_kg'], inplace = True)

test_input = test_dataset.copy()
test_input.drop(columns = ['weight_kg'], inplace = True)


train_labels = train_dataset.copy()
train_labels.drop(columns = ['trade_usd'], inplace = True)

test_labels = test_dataset.copy()
test_labels.drop(columns = ['trade_usd'], inplace = True)

max_usd = df.trade_usd.max()
min_usd = df.trade_usd.min()
normed_train_data = (train_input - min_usd) / (max_usd - min_usd)
normed_test_data = (test_input - min_usd) / (max_usd - min_usd)
print(normed_train_data)
print(normed_test_data)


def build_model():
  model = keras.Sequential([
    layers.Dense(5, activation='sigmoid', input_shape=[len(train_input.keys())]),
    layers.Dense(5, activation='sigmoid'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.01)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model

model = build_model()

model.summary()

class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    if epoch % 100 == 0: print('')
    print('.', end='')

EPOCHS = 4000

print("train_labels")
print(train_labels)
print("train_labels")

history = model.fit(
  normed_train_data, train_labels,
  epochs=EPOCHS, validation_split = 0.2, verbose=0,
  callbacks=[PrintDot()])

print()
hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print(hist.tail(40))

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()
  plt.show()


plot_history(history)


model = build_model()

# Параметр patience определяет количество эпох, проверяемых на улучшение
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

history = model.fit(normed_train_data, train_labels, epochs=EPOCHS,
                    validation_split = 0.2, verbose=0, callbacks=[early_stop, PrintDot()])

plot_history(history)

loss, mae, mse = model.evaluate(normed_test_data, test_labels, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mae))


test_a = list()
test_a.append(0.5)
test_a.append(0)
test_a.append(1)

test_predictions = model.predict(normed_test_data).flatten()

predict = test_labels.copy()
predict['weight_kg'] = test_predictions

def plot_graph():
  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(test_labels,
           label = 'True ')
  plt.plot(predict,
           label = 'predict')
  plt.ylim([0,1])
  plt.legend()
  plt.show()

plot_graph()

plt.scatter(test_labels, test_predictions)
plt.xlabel('True Values [MPG]')
plt.ylabel('Predictions [MPG]')
plt.axis('equal')
plt.axis('square')
plt.xlim([0,plt.xlim()[1]])
plt.ylim([0,plt.ylim()[1]])
_ = plt.plot([-100, 100], [-100, 100])

plt.show()

error = test_predictions - test_labels
plt.hist(error, bins = 25)
plt.xlabel("Prediction Error [MPG]")
_ = plt.ylabel("Count")
plt.show()
