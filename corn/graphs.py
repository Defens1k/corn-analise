import pathlib

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers

import inspect

neurons = 64

print(tf.__version__)

df = pd.read_csv('../input/10_ceras.csv')


def weight_year(data):
    data = data[data['year'] != 2019]
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("Общий график зависимости веса от года")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def usd_year(data):
    data = data[data['year'] != 2019]
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("Общий график зависимости денег от года")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")

#russian block

def russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по России')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по России')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости денег от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")



def export_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по России')
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса экспорта от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def export_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по России')
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости денег экспорта от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_export_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Export']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по России')

    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса экспорта по подгруппам от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_export_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Export']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по России')

    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости Денег экспорта по подгруппам от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_import_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Import']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Import']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по России')

    plt.plot(df_export, label = 'импорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса импорта по подгруппам от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_import_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] == 'Russian Federation']

    df_export = data[data['flow'] == 'Import']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Import']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по России')

    plt.plot(df_export, label = 'импорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости Денег импорта по подгруппам от года по России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")

#not russian block

def not_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по всем странам, кроме России')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def not_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']
    df_sum = data.copy()
    df_sum = df_sum.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_sum.drop(columns = ['quantity'], inplace = True)


    df_import = data[data['flow'] == 'Import']
    df_import = df_import.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_import.drop(columns = ['quantity'], inplace = True)

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по всем странам, кроме России')
    plt.plot(df_import, label = 'импорт', linewidth = 4)
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.plot(df_sum, label = 'всего', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости денег от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")



def export_not_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по всем странам, кроме России')
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса экспорта от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def export_not_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по всем странам, кроме России')
    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости денег экспорта от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_export_not_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Export']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по всем странам, кроме России')

    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса экспорта по подгруппам от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_export_not_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Export']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Export']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по всем странам, кроме России')

    plt.plot(df_export, label = 'экспорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости Денег экспорта по подгруппам от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_import_not_russian_weight_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Import']
    df_export = df_export.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Import']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['weight_kg','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Вес по всем странам, кроме России')

    plt.plot(df_export, label = 'импорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости веса импорта по подгруппам от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")


def commodity_import_not_russian_usd_year(data):
    data = data[data['year'] != 2019]
    data = data[data['country_or_area'] != 'Russian Federation']

    df_export = data[data['flow'] == 'Import']
    df_export = df_export.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
    df_export.drop(columns = ['quantity'], inplace = True)

    commodity_exports = {}
    for commodity in data['commodity'].unique():
        value = data[data['flow'] == 'Import']
        value = value[data['commodity'] == commodity]
        value = value.groupby(['year'],as_index=True)['trade_usd','quantity'].agg('sum')
        value.drop(columns = ['quantity'], inplace = True)
        commodity_exports[commodity] = value

    plt.figure(figsize=(16,9))
    plt.xlabel('Год')
    plt.ylabel('Деньги по всем странам, кроме России')

    plt.plot(df_export, label = 'импорт', linewidth = 4)
    for i in commodity_exports.keys():
        plt.plot(commodity_exports[i], label = i, linewidth = 4)

    plt.minorticks_on()
    plt.grid(which='minor', 
        color = 'k', 
        linestyle = ':')
    plt.grid(which='major',
        color = 'k', 
        linewidth = 1)
    plt.legend()
    plt.title("График зависимости Денег импорта по подгруппам от года по всем странам, кроме России")
    plt.savefig("../out/" + str(inspect.currentframe()).split()[-1][0:-1:1] + ".png")





weight_year(df)
usd_year(df)

#russian block
russian_weight_year(df)
russian_usd_year(df)
export_russian_weight_year(df)
export_russian_usd_year(df)
commodity_export_russian_weight_year(df)
commodity_export_russian_usd_year(df)
commodity_import_russian_weight_year(df)
commodity_import_russian_usd_year(df)

#not russian block
not_russian_weight_year(df)
not_russian_usd_year(df)
export_not_russian_weight_year(df)
export_not_russian_usd_year(df)
commodity_export_not_russian_weight_year(df)
commodity_export_not_russian_usd_year(df)
commodity_import_not_russian_weight_year(df)
commodity_import_not_russian_usd_year(df)



plt.show()
exit()

df.drop(columns = ['commodity'], inplace = True)
df.drop(columns = ['quantity_name'], inplace = True)
df.drop(columns = ['quantity'], inplace = True)
df.drop(columns = ['country_or_area'], inplace = True)
df.drop(columns = ['year'], inplace = True)
df.drop(columns = ['flow'], inplace = True)

max_kg = df.weight_kg.max()
min_kg = df.weight_kg.min()
new_kg = list()
for i in df['weight_kg']:
  new_kg.append(10 * i / max_kg)
#new_kg = new_kg.sort()
df['weight_kg'] = new_kg

df['weight_kg'].plot(figsize=(12,6))
plt.show()


print("max_kg")
print(max_kg)
print(min_kg)
print("max_kg")


train_dataset = df.sample(frac=0.8,random_state=0)
test_dataset = df.drop(train_dataset.index)


train_input = train_dataset.copy()
train_input.drop(columns = ['weight_kg'], inplace = True)

test_input = test_dataset.copy()
test_input.drop(columns = ['weight_kg'], inplace = True)


train_labels = train_dataset.copy()
train_labels.drop(columns = ['trade_usd'], inplace = True)

test_labels = test_dataset.copy()
test_labels.drop(columns = ['trade_usd'], inplace = True)

max_usd = df.trade_usd.max()
min_usd = df.trade_usd.min()
normed_train_data = (train_input - min_usd) / (max_usd - min_usd)
normed_test_data = (test_input - min_usd) / (max_usd - min_usd)
print(normed_train_data)
print(normed_test_data)


def build_model():
  model = keras.Sequential([
    layers.Dense(5, activation='sigmoid', input_shape=[len(train_input.keys())]),
    layers.Dense(5, activation='sigmoid'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.01)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model

model = build_model()

model.summary()

class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    if epoch % 100 == 0: print('')
    print('.', end='')

EPOCHS = 40

print("train_labels")
print(train_labels)
print("train_labels")

history = model.fit(
  normed_train_data, train_labels,
  epochs=EPOCHS, validation_split = 0.2, verbose=0,
  callbacks=[PrintDot()])


hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print(hist.tail(40))

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure(figsize=(16,9))
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()
  plt.show()


plot_history(history)


model = build_model()

# Параметр patience определяет количество эпох, проверяемых на улучшение
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

history = model.fit(normed_train_data, train_labels, epochs=EPOCHS,
                    validation_split = 0.2, verbose=0, callbacks=[early_stop, PrintDot()])

plot_history(history)

loss, mae, mse = model.evaluate(normed_test_data, test_labels, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mae))


test_a = list()
test_a.append(0.5)
test_a.append(0)
test_a.append(1)

test_predictions = model.predict(normed_test_data).flatten()

predict = test_labels.copy()
predict['weight_kg'] = test_predictions

def plot_graph():
  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(test_labels,
           label = 'True ')
  plt.plot(predict,
           label = 'predict')
  plt.ylim([0,1])
  plt.legend()
  plt.show()

plot_graph()

plt.scatter(test_labels, test_predictions)
plt.xlabel('True Values [MPG]')
plt.ylabel('Predictions [MPG]')
plt.axis('equal')
plt.axis('square')
plt.xlim([0,plt.xlim()[1]])
plt.ylim([0,plt.ylim()[1]])
_ = plt.plot([-100, 100], [-100, 100])

plt.show()

error = test_predictions - test_labels
plt.hist(error, bins = 25)
plt.xlabel("Prediction Error [MPG]")
_ = plt.ylabel("Count")
plt.show()
