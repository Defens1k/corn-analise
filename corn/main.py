import pathlib

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers

neurons = 64

print(tf.__version__)

#df = pd.read_csv('../input/commodity_trade_statistics_data.csv')
df = pd.read_csv('../input/10_ceras.csv')


df = df[df['country_or_area'] == 'Russian Federation']

df.drop(columns = ['commodity'], inplace = True)
df.drop(columns = ['quantity_name'], inplace = True)
df.drop(columns = ['quantity'], inplace = True)
df.drop(columns = ['country_or_area'], inplace = True)
df.drop(columns = ['year'], inplace = True)
df.drop(columns = ['flow'], inplace = True)

max_kg = df.weight_kg.max()
min_kg = df.weight_kg.min()
new_kg = list()
for i in df['weight_kg']:
  new_kg.append(10 * i / max_kg)
#new_kg = new_kg.sort()
df['weight_kg'] = new_kg

df['weight_kg'].plot(figsize=(12,6))
plt.show()


print("max_kg")
print(max_kg)
print(min_kg)
print("max_kg")


train_dataset = df.sample(frac=0.8,random_state=0)
test_dataset = df.drop(train_dataset.index)


train_input = train_dataset.copy()
train_input.drop(columns = ['weight_kg'], inplace = True)

test_input = test_dataset.copy()
test_input.drop(columns = ['weight_kg'], inplace = True)


train_labels = train_dataset.copy()
train_labels.drop(columns = ['trade_usd'], inplace = True)

test_labels = test_dataset.copy()
test_labels.drop(columns = ['trade_usd'], inplace = True)

max_usd = df.trade_usd.max()
min_usd = df.trade_usd.min()
normed_train_data = (train_input - min_usd) / (max_usd - min_usd)
normed_test_data = (test_input - min_usd) / (max_usd - min_usd)
print(normed_train_data)
print(normed_test_data)


def build_model():
  model = keras.Sequential([
    layers.Dense(5, activation='sigmoid', input_shape=[len(train_input.keys())]),
    layers.Dense(5, activation='sigmoid'),
    layers.Dense(1)
  ])

  optimizer = tf.keras.optimizers.RMSprop(0.01)

  model.compile(loss='mse',
                optimizer=optimizer,
                metrics=['mae', 'mse'])
  return model

model = build_model()

model.summary()

class PrintDot(keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs):
    if epoch % 100 == 0: print('')
    print('.', end='')

EPOCHS = 40

print("train_labels")
print(train_labels)
print("train_labels")

history = model.fit(
  normed_train_data, train_labels,
  epochs=EPOCHS, validation_split = 0.2, verbose=0,
  callbacks=[PrintDot()])


hist = pd.DataFrame(history.history)
hist['epoch'] = history.epoch
print(hist.tail(40))

def plot_history(history):
  hist = pd.DataFrame(history.history)
  hist['epoch'] = history.epoch

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(hist['epoch'], hist['mae'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mae'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()

  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Square Error [$MPG^2$]')
  plt.plot(hist['epoch'], hist['mse'],
           label='Train Error')
  plt.plot(hist['epoch'], hist['val_mse'],
           label = 'Val Error')
  plt.ylim([0,1])
  plt.legend()
  plt.show()


plot_history(history)


model = build_model()

# Параметр patience определяет количество эпох, проверяемых на улучшение
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)

history = model.fit(normed_train_data, train_labels, epochs=EPOCHS,
                    validation_split = 0.2, verbose=0, callbacks=[early_stop, PrintDot()])

plot_history(history)

loss, mae, mse = model.evaluate(normed_test_data, test_labels, verbose=2)

print("Testing set Mean Abs Error: {:5.2f} MPG".format(mae))


test_a = list()
test_a.append(0.5)
test_a.append(0)
test_a.append(1)

test_predictions = model.predict(normed_test_data).flatten()

predict = test_labels.copy()
predict['weight_kg'] = test_predictions

def plot_graph():
  plt.figure()
  plt.xlabel('Epoch')
  plt.ylabel('Mean Abs Error [MPG]')
  plt.plot(test_labels,
           label = 'True ')
  plt.plot(predict,
           label = 'predict')
  plt.ylim([0,1])
  plt.legend()
  plt.show()

plot_graph()

plt.scatter(test_labels, test_predictions)
plt.xlabel('True Values [MPG]')
plt.ylabel('Predictions [MPG]')
plt.axis('equal')
plt.axis('square')
plt.xlim([0,plt.xlim()[1]])
plt.ylim([0,plt.ylim()[1]])
_ = plt.plot([-100, 100], [-100, 100])

plt.show()

error = test_predictions - test_labels
plt.hist(error, bins = 25)
plt.xlabel("Prediction Error [MPG]")
_ = plt.ylabel("Count")
plt.show()
